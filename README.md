# Git Docs using markdown

Here are some notes about `git` using the text-based `markdown` syntax. Good `markdown` docs include things like:

- <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>
- <https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf>

Here is a good online markdown editor to play around with:

- <https://dillinger.io/>

# Git Overview

`git` is a ....

# Git Commands

Here is a header table and then command descriptions.

Command | Brief Description
--------|------------------
clone   | clone an existing repo

## clone

git clone is used to ...


# Potential future work

Convert to ascidoc for more complete doc features
